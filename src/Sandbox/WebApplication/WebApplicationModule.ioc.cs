﻿using Autofac;
using Sandbox.Core.Services;
using Sandbox.Core.Services.EntityFramework;
using WebApplication.CategoryWS;

namespace WebApplication
{
    public class WebApplicationModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            builder.RegisterType<ProductService>()
                .As<IProductService>()
                ;

            builder.RegisterType<CategoryWebServiceClient>()
                .As<ICategoryWebService>()
                ;
        }
    }
}