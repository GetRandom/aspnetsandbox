﻿using Sandbox.Core;
using Sandbox.Core.Services;
using Sandbox.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;

namespace WebApplication.Controllers
{
    public class ProductsController : ApiController
    {
        private readonly IProductService _productService;
        private readonly Func<Product, ProductDto> _map = p => new ProductDto
        {
            Id = p.Id,
            Name = p.Name
        };

        public ProductsController(IProductService productService)
        {
            _productService = productService;
        }

        [Route("api/categories/{categoryId}/products")]
        public async Task<IEnumerable<ProductDto>> GetProducts(int categoryId, [FromUri]int page, [FromUri]int countPerPage)
        {
            var products = await _productService.GetProductsAsync(categoryId, page, countPerPage);
            var dtos = products.Select(_map);
            return dtos;
        }
    }
}