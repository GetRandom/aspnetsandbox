﻿using System.Threading.Tasks;
using System.Web.Http;
using WebApplication.CategoryWS;

namespace WebApplication.Controllers
{
    public class CategoriesController : ApiController
    {
        private readonly ICategoryWebService _categoryService;

        public CategoriesController(ICategoryWebService categoryService)
        {
            _categoryService = categoryService;
        }
        
        [Route("api/categories")]
        public async Task<IHttpActionResult> Get([FromUri]string key)
        {
            var category = await _categoryService.GetCategoryAsync(key);

            if (category == null) return NotFound();

            return Ok(category);
        }
    }
}