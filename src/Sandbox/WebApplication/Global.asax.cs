﻿using Autofac;
using Autofac.Integration.WebApi;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Reflection;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;

namespace WebApplication
{
    public class WebApiApplication : HttpApplication
    {
        protected void Application_Start()
        {
            var builder = new ContainerBuilder();
            builder.RegisterModule<WebApplicationModule>();
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());

            var container = builder.Build();

            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);

            var config = GlobalConfiguration.Configuration;
            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);

            // You may pass application/json or application/xml to Accept header to get suitable response format.
            // It is a default behaviour of ASP.NET Web API. No necessity to use explicit configuration.
            //config.Formatters.XmlFormatter.MediaTypeMappings.Add(new RequestHeaderMapping("Accept", "json", System.StringComparison.InvariantCultureIgnoreCase, false, "application/json"));
            //config.Formatters.XmlFormatter.MediaTypeMappings.Add(new RequestHeaderMapping("Accept", "xml", System.StringComparison.InvariantCultureIgnoreCase, false, "application/xml"));
        }
    }
}
