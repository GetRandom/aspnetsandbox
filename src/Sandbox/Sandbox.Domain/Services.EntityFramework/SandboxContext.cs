﻿using System.Data.Entity;

namespace Sandbox.Core.Services.EntityFramework
{
    public class SandboxContext : DbContext
    {
        public SandboxContext()
            : base("Name=SandboxContext")
        {
            //Database.CompatibleWithModel(true);
        }

        public virtual DbSet<Product> Products { get; set; }
        public virtual DbSet<Category> Categories { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //modelBuilder.Configurations.Add(new SomeMap());
        }
    }
}