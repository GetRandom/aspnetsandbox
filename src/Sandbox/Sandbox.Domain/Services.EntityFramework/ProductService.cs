﻿using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace Sandbox.Core.Services.EntityFramework
{
    public class ProductService : IProductService
    {
        public async Task<IEnumerable<Product>> GetProductsAsync(int categoryId, int page, int countPerPage)
        {
            using (var context = new SandboxContext())
            {
                var limit = countPerPage;
                var offset = (page - 1) * countPerPage;
                var products = await GetProducts(categoryId, limit, offset, context)
                    .ToListAsync()
                    .ConfigureAwait(false);
                return products;
            }
        }

        private DbRawSqlQuery<Product> GetProducts(int categoryId, int limit, int offset, SandboxContext context)
        {
            var categoryIdParameter = new SqlParameter("@categoryId", categoryId);
            var limitParameter = new SqlParameter("@limit", limit);
            var offsetParameter = new SqlParameter("@offset", offset);

            var result = context.Database.SqlQuery<Product>("exec GetProducts @categoryId , @limit, @offset", categoryIdParameter, limitParameter, offsetParameter);
            return result;
        }
    }
}