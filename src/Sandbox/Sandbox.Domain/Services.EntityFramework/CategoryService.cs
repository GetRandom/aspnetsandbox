﻿using System.Data.Entity;
using System.Threading.Tasks;

namespace Sandbox.Core.Services.EntityFramework
{
    public class CategoryService : ICategoryService
    {
        public async Task<Category> GetCategoryAsync(string key)
        {
            using (var context = new SandboxContext())
            {
                var category = await context.Categories.FirstOrDefaultAsync(c => c.Key == key)
                    .ConfigureAwait(false);
                return category;
            }
        }
    }
}