﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Sandbox.Core.Services
{
    public interface IProductService
    {
        Task<IEnumerable<Product>> GetProductsAsync(int categoryId, int page, int countPerPage);
    }
}