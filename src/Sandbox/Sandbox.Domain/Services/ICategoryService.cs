﻿using System.Threading.Tasks;

namespace Sandbox.Core.Services
{
    public interface ICategoryService
    {
        Task<Category> GetCategoryAsync(string key);
    }
}