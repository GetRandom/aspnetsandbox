﻿using System.Collections.Generic;

namespace Sandbox.Core
{
    public class Category
    {
        public int Id { get; set; }

        public string Key { get; set; }

        public string Description { get; set; }

        public ICollection<Product> Products { get; set; }
    }
}