namespace Sandbox.Core.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Sandbox.Core.Services.EntityFramework.SandboxContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Sandbox.Core.Services.EntityFramework.SandboxContext context)
        {
            //  This method will be called after migrating to the latest version.

            var c1 = new Category { Id = 1, Key = "phones", Description = "Description of Phones category." };
            var c2 = new Category { Id = 2, Key = "laptops", Description = "Description of Laptops category." };
            var c3 = new Category { Id = 3, Key = "accessories", Description = "Description of Accessories category." };

            context.Categories.AddOrUpdate(c => c.Id, c1, c2, c3);

            context.SaveChanges();

            context.Products.AddOrUpdate(
                p => p.Id,
                new Product { Id = 1, Name = "Meizu", Category = c1 },
                new Product { Id = 2, Name = "iPhone", Category = c1 },
                new Product { Id = 3, Name = "Xiaomi", Category = c2 },
                new Product { Id = 4, Name = "Dell", Category = c2 },
                new Product { Id = 5, Name = "Meizu headphones", Category = c3 },
                new Product { Id = 6, Name = "EarPods", Category = c3 }
            );

            context.SaveChanges();
        }
    }
}
