﻿using Sandbox.Core;
using Sandbox.Core.Services;
using Sandbox.DTO;
using System;
using System.Threading.Tasks;

namespace Sandbox.WcfService
{
    public class CategoryWebService : ICategoryWebService
    {
        private readonly ICategoryService _categoryService;
        private readonly Func<Category, CategoryDto> _map = c => new CategoryDto
        {
            Id = c.Id,
            Key = c.Key,
            Description = c.Description
        };

        public CategoryWebService(ICategoryService categoryService)
        {
            _categoryService = categoryService;
        }

        public async Task<CategoryDto> GetCategoryAsync(string key)
        {
            var category = await _categoryService.GetCategoryAsync(key);
            var categoryDto = category == null ? null : _map(category);
            return categoryDto;
        }
    }
}