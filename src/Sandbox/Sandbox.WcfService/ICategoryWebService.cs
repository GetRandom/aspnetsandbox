﻿using Sandbox.DTO;
using System.ServiceModel;
using System.Threading.Tasks;

namespace Sandbox.WcfService
{
    [ServiceContract]
    public interface ICategoryWebService
    {
        [OperationContract]
        Task<CategoryDto> GetCategoryAsync(string key);
    }
}