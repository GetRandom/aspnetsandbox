﻿using Autofac;
using Sandbox.Core.Services;
using Sandbox.Core.Services.EntityFramework;

namespace Sandbox.WcfService
{
    public class WcfServiceModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            builder.RegisterType<CategoryService>()
                .As<ICategoryService>()
                ;

            builder.RegisterType<CategoryWebService>()
                .As<ICategoryWebService>()
                ;
        }
    }
}