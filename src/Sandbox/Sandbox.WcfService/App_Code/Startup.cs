﻿using Autofac;
using Autofac.Integration.Wcf;

namespace Sandbox.WcfService.App_Code
{
    public class Startup
    {
        public static void AppInitialize()
        {
            var builder = new ContainerBuilder();

            builder.RegisterModule<WcfServiceModule>();

            var container = builder.Build();

            AutofacHostFactory.Container = container;
        }
    }
}