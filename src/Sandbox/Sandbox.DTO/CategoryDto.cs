﻿namespace Sandbox.DTO
{
    public class CategoryDto
    {
        public int Id { get; set; }

        public string Key { get; set; }

        public string Description { get; set; }
    }
}