To raise the project:

1. Change connection strings in the next places WebApplication/Web.config, WcfService/Web.config, Core/App.config (opt. to run migrations).

2. Execute **Update-Database** in PM Console.

3. Check corresponding URIs in configs and project's settings.

Stored procedure is stored in Services.EntityFramework/StoredProcedures.resx